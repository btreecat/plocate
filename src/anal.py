from PIL import Image
from os import listdir
import colorsys

#Adjustables 
IMG_DIR = "images"
HUE_UPPER_MIN = 350
HUE_LOWER_MAX = 6
GROUP_SIZE = 1
SAT_MIN = 56
VAL_MIN = 50
RED_MIN = 100


imgs = [x for x in listdir(IMG_DIR) if ".jpg" in x.lower()]

print("File Name,Red Count")
for img in imgs:
    im = Image.open(f"{IMG_DIR}/{img}")
    pixel_count = im.height*im.width
    
    colors = im.getcolors(pixel_count)
    
    hsv_colors = []
    for color in colors:
        hsv_data = colorsys.rgb_to_hsv(color[1][0]/255, color[1][1]/255, color[1][2]/255)
        hue = round(hsv_data[0]*360)
        sat = round(hsv_data[1]*100)
        val = round(hsv_data[2]*100)
        if hue >= HUE_UPPER_MIN or hue <= HUE_LOWER_MAX:
            if color[0] >= GROUP_SIZE:
                if SAT_MIN <= sat and VAL_MIN <= val: 
                    hsv_colors.append((color[0],(hue, sat, val))) 

                
    total_red = [x[0] for x in hsv_colors]
    red_count = sum(total_red)

    prct_red = (red_count/pixel_count) * 100
    rounded = round(prct_red, 2)
    im.close()
    #if rounded > 0 :
    #    print(f"File Name: {img} | Red %: {rounded}")
    if red_count >= RED_MIN:
        print(f"{img},{red_count}")
    
    
